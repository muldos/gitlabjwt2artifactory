package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"

	"github.com/lestrrat-go/jwx/jwk"
	"github.com/lestrrat-go/jwx/jwt"
)

/*
 * Helper function to get environment variables
 */
func getEnv(name string) string {
	v := os.Getenv(name)
	if v == "" {
		log.Fatalf("the %s environment variable must be set", name)
	}
	return v
}

/*
 * Helper function to get the generated token expiry time in seconds.
 */

func getExpiryValue() int {
	v, err := strconv.Atoi(getEnv("RT_TOKEN_EXPIRY"))
	if v == 0 || err != nil {
		v = 600 // default value is 10mn
	}
	return v
}

/*
 * Helper function to get environment variables dump to stdout all the JWT token's claims if needed
 */
func dumpClaims(claims map[string]interface{}) {
	keys := make([]string, 0, len(claims))
	for k := range claims {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		v := claims[k]
		log.Printf("jwt claim: %s=%v", k, v)
	}
}

/*
* The JSON structure that will be used as payload for the Short-lived token generation using our REST API
 */
type AccessTokenPayload struct {
	Scope       string `json:"scope"`
	Username    string `json:"username"`
	Description string `json:"description"`
	Expiry      int    `json:"expires_in"`
}

func main() {
	ciJobJWT := getEnv("CI_JOB_JWT")
	jwksURL := fmt.Sprintf("%s/-/jwks", getEnv("CI_SERVER_URL"))
	boundIssuer := getEnv("CI_SERVER_HOST")
	tokenEndpoint := getEnv("RT_TOKEN_GATEWAY_ENDPOINT")
	// Now we will fetch the gitlab jwt key set.
	// One of them should match the "kid" in the JWT token header
	// a key set is public object alike:
	//
	// 		{
	// 			"keys": [
	// 				{
	// 					"kty": "RSA",
	// 					"kid": "_2nr4525S5ArP0KNXCLrH6p0n3auC_DYqPIuO37h3NA",
	// 					"e": "AQAB",
	// 					"n": "rYyQl7zEPEQP8Sow9ZD5f9DcSIWptc..truncated....O8k86DX14Qk4ZBR5m4A0L5lh3hPKE0Wk",
	// 					"use": "sig",
	// 					"alg": "RS256"
	// 				}
	// 			]
	// 		}
	log.Printf("Getting the GitLab JWT public key set from the jwks endpoint at %s...", jwksURL)
	keySet, err := jwk.Fetch(context.Background(), jwksURL)
	if err != nil {
		log.Fatalf("failed to parse JWK from %s: %v", jwksURL, err)
	}
	if keySet.Len() < 1 {
		log.Fatalf("%s did not return any key", jwksURL)
	}

	// parse and validate the Gitlab job's jwt token against the gitlab jwt key set.
	//
	// a job jwt is a base64 encoded string like:
	//
	// 		eyJhbGciOiJSUzI1NiIsImtpZCI6Il8ybnI0NTI1UzVBclAwS05YQ0xySDZwMG4zYXVDX0RZcVBJdU8zN2gzTkEiLCJ0eXAiOiJKV1QifQ.eyJuYW1lc3BhY2VfaWQiOiIxMCIsIm5hbWVzcGFjZV9wYXRoIjoiZXhhbXBsZSIsInByb2plY3RfaWQiOiI3IiwicHJvamVjdF9wYXRoIjoiZXhhbXBsZS9naXRsYWItY2ktdmFsaWRhdGUtand0IiwidXNlcl9pZCI6IjEiLCJ1c2VyX2xvZ2luIjoicm9vdCIsInVzZXJfZW1haWwiOiJhZG1pbkBleGFtcGxlLmNvbSIsInBpcGVsaW5lX2lkIjoiMTIiLCJqb2JfaWQiOiIyMyIsInJlZiI6Im1hc3RlciIsInJlZl90eXBlIjoiYnJhbmNoIiwicmVmX3Byb3RlY3RlZCI6InRydWUiLCJqdGkiOiJlY2I3YjJhOS02ZTljLTQ4NmUtYmYxNC1mNjIyOTgyOTMwODAiLCJpc3MiOiJnaXRsYWIuZXhhbXBsZS5jb20iLCJpYXQiOjE2MDA1OTExMjgsIm5iZiI6MTYwMDU5MTEyMywiZXhwIjoxNjAwNTk0NzI4LCJzdWIiOiJqb2JfMjMifQ.O_5PjdarFNJQ1u8Xh17BoWdsrxHtmeKu8_GJHJVuFRG3PE66hDTC0cOrqCP4iGp5InygIp26DE-C-fJ1QzgAiCkROQY83vLCq3_aTDVozCpuKdvifg7rxM5kd9ZmccmLnRrSnMPFF3LZPxvwn8A50ajJJOEbdD1Cud_lJd5ViVYZRPaATy44gPTFC72yqBIFwsrl5cB5Tlir_iMQyY4iMNYj-OWHG--hMVovUVVr9lFmhU8CmcaWjEd7C9gngp7hQ-BqMTWqhnCUUcipy7hNeHEACTrYjARuJEKAUMQf_23p1WO_ELHBNGrKSrKDFWtY_VOuGi7nmNVXU-Af0HCPzeYcoDwX1ex6E8ucrH5cgwj0exOIknBrcROWrxd6OFGQLo7V0hwRJ5P6auZJr5lG_hc0n2Ijc-sr266LRBzgwrqcVD9pcgfr6hW1wuyt9fyuNDvnXSkNQFT4v_CjhByUHm13CNRm7WW2urVUSL_suKR5yjV1k1AAzHo3-x1SeH4e9J8RkWiAtRGkU3imPtaADR3FpHCSzkncp-DC4iRTtGIKVLLuaLNZqKQWtfbTT8bfP0PxV109sb404t7U_gXZ5cqgi8Jam0FoYUyO_qEuBwwQdyHsj1YvYFCBLIFz3Zcu7gfUgEjGHCcFyrr9SArlj5YUWMmnbns77B0mwvl0Y4M
	//
	// and once decoded, it contains a header, a signature, and a main body, containing "claims":
	//
	// 		header:
	//
	// 			{
	// 				"alg": "RS256",
	// 				"kid": "_2nr4525S5ArP0KNXCLrH6p0n3auC_DYqPIuO37h3NA",
	// 				"typ": "JWT"
	// 			}
	//
	//		payload:
	//
	// 			{
	// 				"namespace_id": "10",
	// 				"namespace_path": "example",
	// 				"project_id": "7",
	// 				"project_path": "example/gitlab-ci-validate-jwt",
	// 				"user_id": "1",
	// 				"user_login": "root",
	// 				"user_email": "admin@example.com",
	// 				"pipeline_id": "12",
	// 				"job_id": "23",
	// 				"ref": "master",
	// 				"ref_type": "branch",
	// 				"ref_protected": "true",
	// 				"jti": "ecb7b2a9-6e9c-486e-bf14-f62298293080",
	// 				"iss": "gitlab.example.com",
	// 				"iat": 1600591128,
	// 				"nbf": 1600591123,
	// 				"exp": 1600594728,
	// 				"sub": "job_23"
	// 			}
	//
	//		signature:
	//
	//			the value is the 3rd part of the jwt.
	//
	//			in this particular example the jwt can be validated with:
	//
	//				RSASHA256(
	//   				base64UrlEncode(header) + "." + base64UrlEncode(payload),
	//					gitLabJwtKeySet.getKey(header.kid))

	log.Println("Validating GitLab CI job JWT...")
	token, err := jwt.ParseString(ciJobJWT, jwt.WithIssuer(boundIssuer), jwt.WithKeySet(keySet))
	if err != nil {
		log.Fatalf("failed to validate the jwt: %v", err)
	}
	claims := token.PrivateClaims()

	log.Printf("jwt is valid for project %s", claims["project_path"])
	log.Printf("user found: %s", claims["user_login"])

	// usually you can also check here the "exp" claims to see if the token is still valid
	// dump the jwt claims if needed (sorted by claim name).
	// dumpClaims(claims)

	// Now call artifactory REST API to generate a short lived token
	// see https://www.jfrog.com/confluence/display/JFROG/JFrog+Platform+REST+API#JFrogPlatformRESTAPI-CreateToken
	url := tokenEndpoint

	payload := AccessTokenPayload{
		Scope:       "applied-permissions/user",
		Username:    fmt.Sprint(claims["user_login"]),
		Description: "created by CI",
		Expiry:      getExpiryValue(),
	}
	// Convert the request payload struct to bytes, ignoring errors
	jsonBody, _ := json.Marshal(payload)
	// Create a new HTTP client and POST request.
	client := &http.Client{}
	// Create a Bearer string by appending string access token
	var bearer = "Bearer " + getEnv("RT_ACCESS_TOKEN")
	// Create a new  POST request using http
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonBody))
	if err != nil {
		fmt.Println(err)
		return
	}
	// add authorization & content type headers to the req
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", bearer)
	// Send the request and retrieve the response.
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	file, err := os.Create("artifactory-token.txt")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()

	// Copy the response body to the file
	_, err = io.Copy(file, resp.Body)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	fmt.Println("File created successfully!")

}
