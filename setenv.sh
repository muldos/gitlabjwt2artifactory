#!/bin/bash

export RT_TOKEN_EXPIRY=600
export RT_TOKEN_GATEWAY_ENDPOINT=https://soleng.jfrog.io/access/api/v1/tokens
export CI_SERVER_HOST=gitlab.com
export CI_SERVER_URL=https://gitlab.com
